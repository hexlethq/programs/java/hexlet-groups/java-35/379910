package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testTake() {
        String[][] image1 = {
                {"*", "*", "*", "*"},
                {"*", " ", " ", "*"},
                {"*", " ", " ", "*"},
                {"*", "*", "*", "*"},
        };
        String[][] expected1 = {
                {"*", "*", "*", "*", "*", "*", "*", "*"},
                {"*", "*", "*", "*", "*", "*", "*", "*"},
                {"*", "*", " ", " ", " ", " ", "*", "*"},
                {"*", "*", " ", " ", " ", " ", "*", "*"},
                {"*", "*", " ", " ", " ", " ", "*", "*"},
                {"*", "*", " ", " ", " ", " ", "*", "*"},
                {"*", "*", "*", "*", "*", "*", "*", "*"},
                {"*", "*", "*", "*", "*", "*", "*", "*"},
        };
        String[][] result1 = App.enlargeArrayImage(image1);
        assertThat(result1).isEqualTo(expected1);

        String[][] image2 = {
                {"*"},
        };
        String[][] expected2 = {
                {"*", "*"},
                {"*", "*"},
        };
        String[][] result2 = App.enlargeArrayImage(image2);
        assertThat(result2).isEqualTo(expected2);


        String[][] image3 = {
                {"1", "1"},
                {"2", "2"},
        };
        String[][] expected3 = {
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"},
                {"2", "2", "2", "2"},
                {"2", "2", "2", "2"},
        };
        String[][] result3 = App.enlargeArrayImage(image3);
        assertThat(result3).isEqualTo(expected3);

    }
}